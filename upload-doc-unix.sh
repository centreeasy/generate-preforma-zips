#!/bin/bash

# Credentials
HOST='sftp://84.88.145.23:22'
USER='www-data'
PASS='Engine-Source'

# Constants NO MODIFICAR
preforma=$(pwd)
apidoc="$preforma/target/apidocs"
commands="$preforma/commandsDoc.txt"

# Check apidocs exists
if [ ! -d "$apidoc" ]; then
	echo "apidocs not found!"
	exit 1;
fi

# Init commands file
echo "cd /easy/www/DPFManager" > $commands
echo "mkdir new-doc" >> $commands
echo "cd new-doc" >> $commands

# Search files & upload
cd $apidoc
# Folders
find . -type d | while read -r folder; do
	if [ "$folder" != "." ]; then
		echo "mkdir $folder" >> $commands
	fi;
done 
echo "cd .." >> $commands
# Files
find . -type f | while read -r name; do
	local=$name
	remote="/easy/www/DPFManager/new-doc/$name"
	echo "put $local -o $remote" >> $commands
done 
echo "bye" >> $commands

lftp -u $USER,$PASS $HOST < $commands

rm $commands
