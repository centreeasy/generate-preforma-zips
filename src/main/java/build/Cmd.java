package build;

import java.io.File;
import java.io.IOException;

/**
 * Created by Adri� Llorens on 04/11/2016.
 */
public class Cmd {

  public static void run(String[] args, File directory) throws InterruptedException, IOException {
    String[] command = new String[args.length+2];
    command[0] = "cmd.exe";
    command[1] = "/c";
    int i = 2;
    for (String arg : args){
      command[i] = arg;
      i++;
    }

    ProcessBuilder builder = new ProcessBuilder(command);
    builder.directory(directory);
    Process p = builder.start();
    p.waitFor();
  }
}
