package build;

import zips.Data;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.SystemUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * Created by Adri� Llorens on 04/11/2016.
 */
public class Builder {

  private File tmp;
  private String m2Home;
  private String basePath;

  private boolean exeMaked;
  private boolean srcMaked;
  private boolean buildMaked;

  public Builder(String p, String m) {
    basePath = p;
    m2Home = m;
    tmp = getNextTmp("target/build");
  }

//  public void gitClone() throws IOException, InterruptedException {
//    System.out.println("Cloning repository");
//    String url = "https://github.com/EasyinnovaSL/DPFManager.git";
//    Cmd.run(new String[]{"git", "clone", url}, new File(""));
//  }
//
//  public void mvnInstall() throws IOException, InterruptedException {
//    System.out.println("Maven install");
//    Cmd.run(new String[]{"mvn", "install", "-DskipTests", "-Dinstallers"}, new File("DPFManager"));
//  }

  public void makeSrc() throws IOException {
    System.out.println("Making folder src");
    // Copy files
    File source = new File(basePath);
    File output = new File(tmp.getPath() + "/src");
    for (File fileSource : source.listFiles()) {
      if (!fileSource.getName().equals("target") && !fileSource.getName().equals(".git") && !fileSource.getName().equals("generate.jar")) {
        if (fileSource.isDirectory()) {
          FileUtils.copyDirectory(fileSource, new File(output.getPath() + "/" + fileSource.getName()));
        } else if (fileSource.isFile()) {
          FileUtils.copyFile(fileSource, new File(output.getPath() + "/" + fileSource.getName()));
        }
      }
    }
    source = new File(basePath + "/target/sources");
    output = new File(tmp.getPath() + "/src/sources");
    FileUtils.copyDirectory(source, output);
    srcMaked = true;
  }

  public void makeExec() throws IOException {
    System.out.println("Making folder exec");
    File javaFolder = new File(basePath + "/target/jfx/java");
    File nativeFolder = new File(basePath + "/target/jfx/native");
    File javaFolder32 = new File(basePath + "/target/jfx/java32");
    File nativeFolder32 = new File(basePath + "/target/jfx/native32");
    if (SystemUtils.IS_OS_WINDOWS) {
      javaFolder = new File(basePath + "/target/jfx/java/bundles");
      nativeFolder = new File(basePath + "/target/jfx/native/bundles");
      javaFolder32 = null;
    }

    for (String num : getNumbersByCurrentSystem()) {
      // Java
      for (File file : javaFolder.listFiles()) {
        if (file.getName().endsWith(getExtensionByNum(num))) {
          String normalName = file.getName().replace("DPF M", "dpf-m");
          File output = new File(tmp.getPath() + "/exec" + num + "/" + normalName);
          FileUtils.copyFile(file, output);
        }
      }
      if (javaFolder32 != null){
        for (File file : javaFolder32.listFiles()) {
          if (file.getName().endsWith(getExtensionByNum(num))) {
            String normalName = file.getName().replace("DPF M", "dpf-m");
            String normal32Name = (!normalName.contains("86")) ? normalName.substring(0, normalName.lastIndexOf(".")) + "-x32" + normalName.substring(normalName.lastIndexOf(".")) : normalName;
            File output = new File(tmp.getPath() + "/exec" + num + "/" + normal32Name);
            FileUtils.copyFile(file, output);
          }
        }
      }
      // Native
      for (File file : nativeFolder.listFiles()) {
        if (file.getName().endsWith(getExtensionByNum(num))) {
          String normalName = file.getName().replace("DPF M", "dpf-m");
          String lightName = normalName.substring(0, normalName.lastIndexOf("-")) + "-light" + normalName.substring(normalName.lastIndexOf("-"));
          File output = new File(tmp.getPath() + "/exec" + num + "/" + lightName);
          FileUtils.copyFile(file, output);
        }
      }
      for (File file : nativeFolder32.listFiles()) {
        if (file.getName().endsWith(getExtensionByNum(num))) {
          String normalName = file.getName().replace("DPF M", "dpf-m");
          String lightName = normalName.substring(0, normalName.lastIndexOf("-")) + "-light" + normalName.substring(normalName.lastIndexOf("-"));
          String light32Name = (!lightName.contains("86")) ? lightName.substring(0, lightName.lastIndexOf(".")) + "-x32" + lightName.substring(lightName.lastIndexOf(".")) : lightName;
          File output = new File(tmp.getPath() + "/exec" + num + "/" + light32Name);
          FileUtils.copyFile(file, output);
        }
      }
    }
    exeMaked = true;
  }

  public void makeBuildenv() throws IOException {
    System.out.println("Making folder buildenv");
    // Copy files
    for (String num : getNumbersByCurrentSystem()) {
      // OS values
      String osName = "";
      if (SystemUtils.IS_OS_WINDOWS) {
        osName += "windows";
      } else if (SystemUtils.IS_OS_MAC) {
        osName += "macos";
      } else {
        osName += "linux";
      }
      File output = new File(tmp.getPath() + "/buildenv" + num);
      output.mkdirs();
      // OpenJFX
      String name = "buildenv/" + osName + ".zip";
      File source = getFileFromResources(name);
      unzipFileIntoDirectory(source, output);
      FileUtils.forceDelete(source);
      // Maven
      String mName = "buildenv/maven.zip";
      File sourceMvn = getFileFromResources(mName);
      unzipFileIntoDirectory(sourceMvn, output);
      FileUtils.forceDelete(sourceMvn);
      // Repository
      File repository = new File(m2Home);
      File repoOutput = new File(output.getPath() + "/maven/repository");
      FileUtils.copyDirectory(repository, repoOutput);
      // Instructions
      String instructions = "instructions/" + osName + ".txt";
      File insSource = getFileFromResources(instructions);
      File insOutput = new File(output.getPath() + "/instructions.txt");
      FileUtils.copyFile(insSource, insOutput);
      FileUtils.forceDelete(insSource);
    }
    buildMaked = true;
  }

  public void zipFolders() throws IOException {
    File tmpZips = getNextTmp("release-zips");
    File tmpInstallers = getNextTmp("release-installers");
    System.out.println("Zipping folders");
    for (String num : getNumbersByCurrentSystem()) {
      File osFolder = new File(tmpZips.getPath() + "/" + getOsFolderByNum(num));
      if (!osFolder.exists()){
        osFolder.mkdirs();
      }
      File osFolderIns = new File(tmpInstallers.getPath() + "/" + getOsFolderByNum(num));
      if (!osFolderIns.exists()){
        osFolderIns.mkdirs();
      }
      File zip, folder, installers;
      // Src
      if (srcMaked){
        zip = new File(osFolder.getPath() + "/src" + num + "-" + Data.getDate() + ".zip");
        folder = new File(tmp.getPath() + "/src");
        zipFolder(folder.getAbsolutePath(), zip.getAbsolutePath());
      }
      // Exec
      if (exeMaked){
        zip = new File(osFolder.getPath() + "/exec" + num + "-" + Data.getDate() + ".zip");
        folder = new File(tmp.getPath() + "/exec" + num);
        zipFolder(folder.getAbsolutePath(), zip.getAbsolutePath());
        // Installers
        folder = new File(tmp.getPath() + "/exec" + num);
        installers = new File(osFolderIns.getPath());
        FileUtils.copyDirectory(folder, installers);
      }
      // Buildenv
      if (buildMaked){
        zip = new File(osFolder.getPath() + "/buildenv" + num + "-" + Data.getDate() + ".zip");
        folder = new File(tmp.getPath() + "/buildenv" + num);
        zipFolder(folder.getAbsolutePath(), zip.getAbsolutePath());
      }
      System.out.println("");
      System.out.println("Release " + osFolder.getName() + ":");
      System.out.println(osFolder.getPath());
    }
    System.out.println("");
  }

  private File getFileFromResources(String name) {
    File outputFileTmp = new File(name);
    File outputFile = new File(outputFileTmp.getName());
    if (outputFile.exists()) {
      outputFile.delete();
    }
    CodeSource src = MainBuild.class.getProtectionDomain().getCodeSource();
    ClassLoader cLoader = MainBuild.class.getClassLoader();
    if (src != null) {
      InputStream in = cLoader.getResourceAsStream(name);
      if (in != null) {
        try {
          int readBytes;
          byte[] buffer = new byte[4096];
          OutputStream resStreamOut = new FileOutputStream(outputFile);
          while ((readBytes = in.read(buffer)) > 0) {
            resStreamOut.write(buffer, 0, readBytes);
          }
          resStreamOut.close();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
    return outputFile;
  }

  private String getExtensionByNum(String num) {
    if (num.equals("01")) {
      return ".exe";
    } else if (num.equals("05")) {
      return ".dmg";
    } else if (num.equals("09") || num.equals("17")) {
      return ".deb";
    } else {
      return ".rpm";
    }
  }

  private List<String> getNumbersByCurrentSystem() {
    List<String> list = new ArrayList<String>();
    if (SystemUtils.IS_OS_WINDOWS) {
      list.add("01");
    } else if (SystemUtils.IS_OS_MAC || SystemUtils.IS_OS_MAC_OSX) {
      list.add("05");
    } else {
      list.add("09");
      list.add("13");
      list.add("17");
      list.add("21");
    }
    return list;
  }

  private String getOsFolderByNum(String num){
    if (num.equals("01")) {
      return "Windows";
    } else if (num.equals("05")) {
      return "MacOS";
    } else if (num.equals("09")) {
      return "Ubuntu";
    } else if (num.equals("13")) {
      return "Fedora";
    } else if (num.equals("17")) {
      return "Debian";
    } else if (num.equals("21")) {
      return "OpenSuse";
    }
    return "";
  }

  /**
   * Zip folder
   *
   * @param folder The folder to zip
   * @param output The output zip
   */
  private boolean zipFolder(String folder, String output) {
    // Check if exists
    if (new File(output).exists() && !new File(output).delete()) {
      return false;
    }
    // Make the zip
    try {
      ZipOutputStream zipFile = new ZipOutputStream(new FileOutputStream(output));
      folder = folder + "/";
      compressDirectoryToZipfile(folder, folder, zipFile);
      IOUtils.closeQuietly(zipFile);
      return true;
    } catch (Exception e) {
      e.printStackTrace();
      return false;
    }
  }

  private void compressDirectoryToZipfile(String rootDir, String sourceDir, ZipOutputStream out) throws IOException, FileNotFoundException {
    for (File file : new File(sourceDir).listFiles()) {
      if (file.isDirectory()) {
        compressDirectoryToZipfile(rootDir, sourceDir + file.getName() + "/", out);
      } else {
        ZipEntry entry = new ZipEntry(sourceDir.replace(rootDir, "") + file.getName());
        out.putNextEntry(entry);

        FileInputStream in = new FileInputStream(sourceDir + file.getName());
        IOUtils.copy(in, out);
        IOUtils.closeQuietly(in);
      }
    }
  }

  private boolean unzipFileIntoDirectory(File file, File dest) {
    try {
      ZipFile zipFile = new ZipFile(file);
      Enumeration files = zipFile.entries();
      while (files.hasMoreElements()) {
        ZipEntry entry = (ZipEntry) files.nextElement();
        InputStream eis = zipFile.getInputStream(entry);
        byte[] buffer = new byte[1024];
        int bytesRead = 0;

        File f = new File(dest.getAbsolutePath() + File.separator + entry.getName());

        if (entry.isDirectory()) {
          f.mkdirs();
          continue;
        } else {
          f.getParentFile().mkdirs();
          f.createNewFile();
        }

        FileOutputStream fos = new FileOutputStream(f);
        while ((bytesRead = eis.read(buffer)) != -1) {
          fos.write(buffer, 0, bytesRead);
        }

        eis.close();
        fos.close();
      }
      zipFile.close();
    } catch (IOException e) {
      return false;
    }
    return true;
  }

  private File getNextTmp(String key) {
    String tmp = key;
    int count = 0;
    while (new File(tmp).exists()) {
      tmp = key + "-" + count;
      count++;
    }
    File file = new File(tmp);
    file.mkdirs();
    return file;
  }

}

