package build;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.CodeSource;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Adri� Llorens on 04/11/2016.
 */
public class MainBuild {

  public static void main(String[] args) {
    String DPFManager = "";
    String m2Home = "";
    List<String> params = Arrays.asList(args);
    if (params.size() == 1) {
      System.out.println("Assuming we are in the project base folder (with mvn install done).");
      try {
        CodeSource codeSource = MainBuild.class.getProtectionDomain().getCodeSource();
        File jarFile = new File(codeSource.getLocation().toURI().getPath());
        DPFManager = jarFile.getParentFile().getPath();
      } catch (Exception e) {
        e.printStackTrace();
      }
      m2Home = params.get(0);
    } else if (params.size() == 2) {
      DPFManager = params.get(0);
      m2Home = params.get(1);
    } else {
      System.out.println("Parameters error!!!");
      showHelp();
    }

    System.out.println("DPFManager folder:    " + DPFManager);
    System.out.println("M2 repository folder: " + m2Home);
    System.out.println("");

    try {
      Builder builder = new Builder(DPFManager, m2Home);

      // Make src zip
      builder.makeSrc();

      // Make exec zip
      builder.makeExec();

      // Make buildenv zip
      builder.makeBuildenv();

      // Make zips and installers
      builder.zipFolders();

      System.out.println("Finished");
    } catch (Exception e) {
      e.printStackTrace();
    }
    return;
  }

  private static void showHelp() {
    System.out.println("Input parameters:");
    System.out.println("   - The base folder of DPFManager project. IMPORTANT, with mvn install done.");
    System.out.println("   - The path to the maven repository folder. E: {User.folder}/.m2/repository");
    System.out.println("The parameters order is IMPORTANT!");
    System.out.println("");
    System.out.println("OR");
    System.out.println("");
    System.out.println("Input parameters:");
    System.out.println("   - The path to the maven repository folder. E: {User.folder}/.m2/repository");
    System.out.println("It will assume that the jar is located in the base folder of the DPFManager project.");
    System.exit(0);
  }

}


