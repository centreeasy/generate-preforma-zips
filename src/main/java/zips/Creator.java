package zips;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * Created by Adri� Llorens on 04/11/2016.
 */
public class Creator implements Runnable {

  private String baseFolder;
  private String outputFolder;
  private Integer num;
  private String type;

  public Creator(String b, String o, Integer n, String t) {
    baseFolder = b;
    outputFolder = o;
    num = n;
    type = t;
  }

  private File getNextTmp() {
    String tmp = "target/zips/tmp";
    int count = 0;
    while (new File(tmp).exists()) {
      tmp = "target/zips/tmp" + count;
      count++;
    }
    File file = new File(tmp);
    file.mkdirs();
    return file;
  }

  public void run() {
    try {
      String zipName = type + Data.prettyNumber(num) + "-" + Data.getDate() + ".zip";
      if (new File(outputFolder + "/" + zipName).exists()){
        System.out.println("Skipping " + zipName);
        return;
      }
      String os = Data.getOsByNumber(num);
      List<String> formats = Data.getFormatsByNumber(num);
      File tmp = getNextTmp();
      System.out.println("Copying " + zipName);
      for (String format : formats) {
        File formatFile = new File(tmp.getPath() + "/" + format);
        formatFile.mkdirs();
        File inputFile = getSourceInput(format, type, os);
        File zipFile = getOnlyZip(inputFile);
        if (zipFile != null) {
          // Copy zip contents
          unzipFileIntoDirectory(zipFile, formatFile);
        } else {
          // Copy entire folder
          FileUtils.copyDirectory(inputFile, formatFile);
        }
      }
      System.out.println("Zipping " + zipName);
      zipFolder(tmp.getAbsolutePath(), outputFolder + "/" + zipName);
      System.out.println("Deleting tmp " + zipName);
      FileUtils.deleteDirectory(tmp);
      System.out.println("Finished " + zipName);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static File getOnlyZip(File inputFile) {
    if (inputFile.listFiles().length == 1) {
      File zip = inputFile.listFiles()[0];
      if (zip.getName().endsWith(".zip")) {
        return zip;
      }
    }
    return null;
  }

  private File getSourceInput(String format, String type, String os) {
    String baseInput = baseFolder + "/" + format + "/" + type + "/";
    String input = getExistingInput(baseInput, os);
    File inputFile = new File(input);
    if (inputFile.exists()) {
      return inputFile;
    }
    return null;
  }

  private String getExistingInput(String baseInput, String os) {
    String input = baseInput + os;
    if (new File(input).exists()) {
      return input;
    }
    if (os.equals("debian") || os.equals("ubuntu")) {
      input = baseInput + "deb";
      if (new File(input).exists()) {
        return input;
      }
    }
    if (os.equals("fedora") || os.equals("opensuse")) {
      input = baseInput + "rpm";
      if (new File(input).exists()) {
        return input;
      }
    }
    if (!os.equals("windows") && !os.equals("macos")) {
      input = baseInput + "linux";
      if (new File(input).exists()) {
        return input;
      }
    }
    return baseInput + "default";
  }

  /**
   * Zip folder
   *
   * @param folder The folder to zip
   * @param output The output zip
   */
  private boolean zipFolder(String folder, String output) {
    // Check if exists
    if (new File(output).exists() && !new File(output).delete()) {
      return false;
    }
    // Make the zip
    try {
      ZipOutputStream zipFile = new ZipOutputStream(new FileOutputStream(output));
      folder = folder + "/";
      compressDirectoryToZipfile(folder, folder, zipFile);
      IOUtils.closeQuietly(zipFile);
      return true;
    } catch (Exception e) {
      e.printStackTrace();
      return false;
    }
  }

  private void compressDirectoryToZipfile(String rootDir, String sourceDir, ZipOutputStream out) throws IOException, FileNotFoundException {
    for (File file : new File(sourceDir).listFiles()) {
      if (file.isDirectory()) {
        compressDirectoryToZipfile(rootDir, sourceDir + file.getName() + "/", out);
      } else {
        ZipEntry entry = new ZipEntry(sourceDir.replace(rootDir, "") + file.getName());
        out.putNextEntry(entry);

        FileInputStream in = new FileInputStream(sourceDir + file.getName());
        IOUtils.copy(in, out);
        IOUtils.closeQuietly(in);
      }
    }
  }

  private boolean unzipFileIntoDirectory(File file, File dest) {
    try {
      ZipFile zipFile = new ZipFile(file);
      Enumeration files = zipFile.entries();
      while (files.hasMoreElements()) {
        ZipEntry entry = (ZipEntry) files.nextElement();
        InputStream eis = zipFile.getInputStream(entry);
        byte[] buffer = new byte[1024];
        int bytesRead = 0;

        File f = new File(dest.getAbsolutePath() + File.separator + entry.getName());

        if (entry.isDirectory()) {
          f.mkdirs();
          continue;
        } else {
          f.getParentFile().mkdirs();
          f.createNewFile();
        }

        FileOutputStream fos = new FileOutputStream(f);

        while ((bytesRead = eis.read(buffer)) != -1) {
          fos.write(buffer, 0, bytesRead);
        }
        fos.close();
      }
    } catch (IOException e) {
      return false;
    }
    return true;
  }

}
