package zips;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Adri� Llorens on 04/11/2016.
 */
public class Data {

  private static Map<Integer, String> numbers = null;
  private static String data = null;

  public static Map<Integer, String> getNumbersMap() {
    if (numbers != null) {
      return numbers;
    }
    numbers = new HashMap<Integer, String>();
    numbers.put(1, "windows");
    numbers.put(5, "macos");
    numbers.put(9, "ubuntu");
    numbers.put(13, "fedora");
    numbers.put(17, "debian");
    numbers.put(21, "opensuse");
    return numbers;
  }

  public static boolean isAccepted(int num) {
    if (num > 24){
      return false;
    }
    return !getNumbersMap().containsKey(num);
  }

  public static List<String> getFormatsByNumber(int num) {
    int text = 2;
    int video = 3;
    int all = 0;
    List<String> formats = new ArrayList<String>();
    formats.add("image");
    int mod = num % 4;
    if (mod == text || mod == all) {
      formats.add("text");
    }
    if (mod == video || mod == all) {
      formats.add("video");
    }
    return formats;
  }

  public static String getOsByNumber(int num) {
    while (getNumbersMap().get(num) == null) {
      num--;
    }
    return getNumbersMap().get(num);
  }

  public static String prettyNumber(int number) {
    if (number < 10) {
      return "0" + number;
    }
    return "" + number;
  }

  public static void setDate(String d) {
    data = d;
  }

  public static String getDate() {
    if (data == null) {
      DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
      Date date = new Date();
      data = dateFormat.format(date);
    }
    return data;
  }
}

