package zips;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Adri� Llorens on 04/11/2016.
 */
public class MainZips {

  public static void main(String[] args) {
    List<String> params = Arrays.asList(args);
    if (params.size() < 2 || params.size() > 3) {
      System.out.println("Specify the base folder and the output folder! Order is important!");
      showHelp();
    }

    String baseFolder = params.get(0);
    String outFolder = params.get(1);
    if (params.size() == 3){
      Data.setDate(params.get(2));
    }

    ExecutorService executor = Executors.newFixedThreadPool(6);
    for (int num = 14; num < 25; num++) { // Max 25!
      if (Data.isAccepted(num)) {
        List<String> types = Arrays.asList("exec", "src", "buildenv");
        for (String type : types) {
          Creator creator = new Creator(baseFolder, outFolder, num,type);
          executor.execute(creator);
        }
      }
    }
    executor.shutdown();
    while (!executor.isTerminated()) {}
    return;
  }

  private static void showHelp() {
    System.out.println("Input parameters:");
    System.out.println("    - The base folder of the files to zip. The files to zip MUST follow a specific structure (see below).");
    System.out.println("    - The output folder for the generated zips.");
    System.out.println("    - The specific date to use (OPTIONAL). Format yyyy-MM-dd");
    System.out.println("The parameters order is IMPORTANT! ");
    System.out.println("");
    System.out.println("");
    System.out.println("- Base Folder");
    System.out.println("   - image");
    System.out.println("      - buildenv");
    System.out.println("      - exec");
    System.out.println("      - src");
    System.out.println("   - text");
    System.out.println("      - buildenv");
    System.out.println("      - exec");
    System.out.println("      - src");
    System.out.println("   - video");
    System.out.println("      - buildenv");
    System.out.println("      - exec");
    System.out.println("      - src");
    System.out.println("");
    System.out.println("Each 'buildenv', 'exec' and 'src' folder:");
    System.out.println(" - debian");
    System.out.println(" - fedora");
    System.out.println(" - macos");
    System.out.println(" - opensuse");
    System.out.println(" - ubuntu");
    System.out.println(" - windows");
    System.out.println("The program will use the specific folder for each system. If the folder is not found, it will search for (in this order):");
    System.out.println(" - linux (Ubuntu, Debian, Fedora and OpenSuse)");
    System.out.println(" - deb (Ubuntu and Debian)");
    System.out.println(" - rpm (Fedora and OpenSuse)");
    System.out.println(" - default (All systems)");
    System.out.println("The files inside the folder can be a single zip file, or a list of files/folders. if there is a single zip, it will unzip its content, so the process will be slower.");
    System.exit(0);
  }

}


