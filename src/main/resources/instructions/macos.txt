1. Check Java version

  java -version

If not installed, or the version is lower than 1.8 then Java 8 needs to be installed.

Java 8 can be installed either with the Oracle JDK or with Open JDK.
The difference between them is that the Oracle version includes JavaFX, but the Open JDK version does not.
To install the Open JDK, follow the instructions in OpenJFX folder.

2. Install Maven

Unzip the maven zip.
Move the unzipped folder to a desired location (/path/to/maven).
Link maven to /usr/local/bin

  sudo ln -s /path/to/maven/bin/mvn /usr/local/bin/mvn
  
3. Set the repository folder

Unzip the repository zip.
Move it to a desired location (/path/to/repository).
Modify the maven settings (settings.xml inside the maven folder) to point to this folder as local repository.

  <localRepository>/path/to/repository</localRepository>

4. Build the project

Go to the source folder and type

  mvn install

Alternatively, for a faster build (without javadoc, testing and gpg singing), you can type

  mvn install -Dmaven.javadoc.skip=true -DskipTests -Dgpg.skip
