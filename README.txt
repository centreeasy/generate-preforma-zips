#
# MainZips
#

Input parameters:
    - The base folder of the files to zip. The files to zip MUST follow a specific structure (see below).
    - The output folder for the generated zips.
    - The specific date to use (OPTIONAL). Format yyyy-MM-dd
The parameters order is IMPORTANT!


Base folder Structure:
- Base Folder
   - image
      - buildenv
      - exec
      - src
   - text
      - buildenv
      - exec
      - src
   - video
      - buildenv
      - exec
      - src

Inside 'buildenv', 'exec' and 'src' folder:
 - debian
 - fedora
 - macos
 - opensuse
 - ubuntu
 - windows
The program will use the specific folder for each system (listed above). If the folder is not found, it will search for (in this order):
 - linux (Ubuntu, Debian, Fedora and OpenSuse)
 - deb (Ubuntu and Debian)
 - rpm (Fedora and OpenSuse)
 - default (All systems)
The files inside the folder can be a single zip file, or a list of files/folders. if there is a single zip, it will unzip its content, so the process will be slower.


#
# MainBuild
#

Input parameters:
    - The base folder of DPFManager project. IMPORTANT, with mvn install done.
    - The path to the maven repository folder. E: {User.folder}/.m2/repository
The parameters order is IMPORTANT!